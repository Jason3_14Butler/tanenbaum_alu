# README #

### What is this repository for? ###

* This is a verilog implementation of the Arithmetic Logic unit from pg. 178 of Tanenbaum's Structured Computer Organization 5th Edition. 
* [Learn Markdown](https://eleccompengineering.files.wordpress.com/2014/07/structured_computer_organization_5th_edition_801_pages_2007_.pdf)

### How do I get set up? ###

Install Icarus Verilog on Ubuntu 16.04 

Make run

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact