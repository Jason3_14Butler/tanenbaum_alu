/*
  © Copywrite Jason Butler 2017 GPL
*/


module fulladdertb;

reg x;
reg y; 
reg cin;
reg enable;
wire cout;
wire A; 

fulladder uut (
.x(x),
.y(y),
.cin(cin),
.enable(enable),
.A(A),
.cout(cout)
);

initial
begin
  $dumpfile("fulladdertb.vcd");
  $dumpvars(0,fulladdertb);
x = 0;
y = 0;
cin = 0;
enable = 0;


#20;x=1;
#20;y=1;
#20;cin=1;
#20;x=0;
#20;y=0;
#20;cin=0;
#20;enable=1;
#20;x=1;
#20;y=1;
#20;cin=1;
#20;x=0;
#20;y=0;
#20;cin=0;
#40;
end


initial
begin
$monitor("time = %2d, x=%1b,y=%1b,cin=%1b,enable=%1b,A=%1b,cout=%1b",
$time,x,y,cin,enable,A,cout);
end

endmodule
