/*
  © Copywrite Jason Butler 2017 GPL
*/

module fulladder
(
  input x,
  input y,
  input cin,
  input enable,
  output reg A,
  output reg cout
);

reg p,q;
always @(x,y,cin)
begin
if(enable)
  assign {cout,A} = cin + y + x;
else
  assign {cout,A} = 0;
end
endmodule
