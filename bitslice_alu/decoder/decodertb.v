/*
  © Copywrite Jason Butler 2017 GPL
*/


module decodertb;

reg [1:0] input1;

wire [3:0] out;
wire carryout;

decoder_case uut (
.input_word(input1),
.output_lines(out)
);

initial
begin
  $dumpfile("decodertb.vcd");
  $dumpvars(0,decodertb);
input1 = 0;
#20; input1=1;
#20; input1=2;
#20; input1=3;
end


initial
begin
$monitor("time = %2d, IN1=%2b,OUT=%4b",
$time,input1,out);
end

endmodule
