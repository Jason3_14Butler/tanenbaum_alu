/*
  © Copywrite Jason Butler 2017 GPL
*/


module decoder_case (
input [1:0] input_word,
output reg [3:0] output_lines
);

always @ (input_word)
begin
  output_lines = 0;
  case (input_word)
    2'h0 : output_lines = 4'h1;
    2'h1 : output_lines = 4'h2;
    2'h2 : output_lines = 4'h4;
    2'h3 : output_lines = 4'h8;
  endcase
end

endmodule
