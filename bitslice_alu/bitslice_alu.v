/*
  © Copywrite Jason Butler 2017 GPL
*/


module bitslice_alu (
input A,
input B,
input invA,
input enA,
input enB,
input carryin,
input [1:0] alu_function,
output alu_output,
output carryout
);

wire input_A;
wire input_B;
wire fulladder_out;
wire [2:0] logic_unit_out;

assign input_A = (A & enA) ^ invA;
assign input_B = (B & enB);
wire [3:0] decoder_out;
decoder_case decoder(alu_function,decoder_out);

fulladder adder(input_A, input_B, carryin, decoder_out[3], fulladder_out,carryout);

logic_unit lu(input_A, input_B,decoder_out[0],decoder_out[1],decoder_out[2],logic_unit_out);
assign alu_output = fulladder_out | logic_unit_out[0] | logic_unit_out[1] | logic_unit_out[2]; 

endmodule
