/*
  © Copywrite Jason Butler 2017 GPL
*/


module logic_unittb;

reg A;
reg B; 
reg function_and;
reg function_or;
reg function_not;
wire [2:0] output_lines;

logic_unit uut (
.A(A),
.B(B),
.function_and(function_and),
.function_or(function_or),
.function_not(function_not),
.output_lines(output_lines)
);

initial
begin
  $dumpfile("logic_unittb.vcd");
  $dumpvars(0,logic_unittb);
A = 0;
B = 0;
function_and = 0;
function_or = 0;
function_not = 0;
#20;A=1; function_or = 1;
#20;A=0; function_or =0; 
#20;B=1; function_or =1;
#20;B=0; function_or =0;
#20;B=1;A=1;
#20;function_or = 1;
#20;function_or =0; function_and=1;
#20;A=0;
#20;B=0;A=1;
#20;function_and = 0; function_not =1;
#20;B=1;
#40;
end


initial
begin
$monitor("time = %2d, A=%1b,B=%1b,function_and=%1b,function_or=%1b,function_not=%1b,OUT=%3b",
$time,A,B,function_and,function_or,function_not,output_lines);
end

endmodule
