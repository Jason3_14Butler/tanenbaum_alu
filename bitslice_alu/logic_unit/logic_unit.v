/*
  © Copywrite Jason Butler 2017 GPL
*/


module logic_unit (
input A,
input B,
input function_and,
input function_or,
input function_not,
output [2:0] output_lines
);

wire A_and_B;
wire A_or_B;
wire not_B;

assign A_and_B = A & B;
assign A_or_B = A | B;
assign not_B = ~B;

assign output_lines[0] = A_and_B & function_and;
assign output_lines[1] = A_or_B & function_or;
assign output_lines[2] = not_B & function_not;

endmodule
