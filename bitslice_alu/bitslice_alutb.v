/*
  © Copywrite Jason Butler 2017 GPL
*/


module fulladdertb;

reg A;
reg B; 
reg invA;
reg enA;
reg enB;
reg carryin;
reg [1:0] alu_function; 
wire alu_output;
wire carryout;

bitslice_alu uut (
.A(A),
.B(B),
.invA(invA),
.enA(enA),
.enB(enB),
.carryin(carryin),
.alu_function(alu_function),
.alu_output(alu_output),
.carryout(carryout)
);

initial
begin
  $dumpfile("fulladdertb.vcd");
  $dumpvars(0,fulladdertb);
A = 0;
B = 0;
invA =0;
enA = 1; enB =1; carryin = 0;
alu_function = 2'h3;

$display("testing adder");
#20;A=1;
#20;B=1;
#20;carryin=1;
#20;A=0;
#20;B=0;
#20;carryin=0;
#20;A=1;
#20;B=1;
#20;carryin=1;
#20;A=0;
#20;B=0;
#20;carryin=0;
#20;
$display("testing logic unit");
$display("tesing AND\n");
#20;alu_function=2'h0;A=0;B=0;
#20;A=1;
#20;A=0;B=1;
#20;A=1;
#20;
$display("testing OR");
#20;alu_function=2'h1;A=0;B=0;
#20;A=1;
#20;A=0;B=1;
#20;A=1;
#20;
$display("test inverter");
#20;alu_function=2'h2;A=0;B=0;
#20;B=1;
#40;
end


initial
begin
$monitor("time = %2d, A=%1b,B=%1b,carryin=%1b,alu_function=%2b,alu_output=%1b,carryout=%1b",
$time,A,B,carryin,alu_function,alu_output,carryout);
end

endmodule
