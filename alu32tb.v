/*
  © Copywrite Jason Butler 2017 GPL
*/

`timescale 1ns / 1ps

module alu32bittb;

reg [31:0] A;
reg [31:0] B; 
reg invA;
reg enA;
reg enB;
reg carryin;
reg [1:0] alu_function; 
wire [31:0] alu_output;
wire carryout;
wire N;
wire Z;

integer i;
integer j;

alu32bit uut (
.A(A),
.B(B),
.invA(invA),
.enA(enA),
.enB(enB),
.carryin(carryin),
.alu_function(alu_function),
.alu_output(alu_output),
.carryout(carryout),
.N(N),
.Z(Z)
);

initial
begin
  $dumpfile("alu32bittb.vcd");
  $dumpvars(0,alu32bittb);
A = 0;
B = 0;
invA =0;
enA = 1; enB =1; carryin = 0;
alu_function = 2'h3;

$display("testing adder");
#20;A=1;
#20;B=1;
#20;carryin=1;
#20;A=0;
#20;B=0;
#20;carryin=0;
#20;A=1;
#20;B=1;
#20;carryin=1;
#20;A=0;
#20;B=0;
#20;carryin=0;

for(i=0; i<'hFF; i=i+1) begin
  #20; A=i;
  for(j=0; j<'hFF; j=j+1) begin
    #20; B=j;
  end
end
#20;
$display("testing logic unit");
$display("tesing AND\n");
#20;alu_function=2'h0;A=0;B=0;
#20;A=1;
#20;A=0;B=1;
#20;A=1;
#20;
$display("testing OR");
#20;alu_function=2'h1;A=0;B=0;
#20;A=1;
#20;A=0;B=1;
#20;A=1;
#20;
$display("test inverter");
#20;alu_function=2'h2;A=0;B=0;
#20;B=1;
$display("test increment");
#20;alu_function=2'h3;enA=0;B=32'hABABABAB;carryin=1; 
#40;
end


initial
begin
$monitor("time = %2d, A=%8h,B=%8h,carryin=%1b,alu_function=%2b,alu_output=%8h,carryout=%1b,N=%1b,Z=%1b ",
$time,A,B,carryin,alu_function,alu_output,carryout,N,Z);
end

endmodule
