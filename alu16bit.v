/*
  © Copywrite Jason Butler 2017 GPL
*/

module alu16bit
(
  input [15:0] A,
  input [15:0] B,
  input invA,
  input enA,
  input enB,
  input carryin,
  input [1:0] alu_function,
  output wire [15:0] alu_output,
  output wire carryout
);

wire [16:0] carries;
wire [32:0] input_temp;

assign carries[0] = carryin;

genvar i;
generate 
  for (i=0; i<16; i =i+1) begin:bitslice_alus
    bitslice_alu s(A[i], B[i],invA,enA,enB,carries[i],alu_function,alu_output[i],carries[i+1]);
end endgenerate
assign carryout = carries[16];
endmodule
