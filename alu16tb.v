/*
  © Copywrite Jason Butler 2017 GPL
*/


module alu16bittb;

reg [15:0] A;
reg [15:0] B; 
reg invA;
reg enA;
reg enB;
reg carryin;
reg [1:0] alu_function; 
wire [15:0] alu_output;
wire carryout;

integer i;
integer j;

alu16bit uut (
.A(A),
.B(B),
.invA(invA),
.enA(enA),
.enB(enB),
.carryin(carryin),
.alu_function(alu_function),
.alu_output(alu_output),
.carryout(carryout)
);

initial
begin
  $dumpfile("alu16bittb.vcd");
  $dumpvars(0,alu16bittb);
A = 0;
B = 0;
invA =0;
enA = 1; enB =1; carryin = 0;
alu_function = 2'h3;

$display("testing adder");
#20;A=1;
#20;B=1;
#20;carryin=1;
#20;A=0;
#20;B=0;
#20;carryin=0;
#20;A=1;
#20;B=1;
#20;carryin=1;
#20;A=0;
#20;B=0;
#20;carryin=0;

for(i=0; i<'hFFFF; i=i+1) begin
  #20; A=i;
  for(j=0; j<'hFFFF; j=j+1) begin
    #20; B=j;
  end
end
#20;
$display("testing logic unit");
$display("tesing AND\n");
#20;alu_function=2'h0;A=0;B=0;
#20;A=1;
#20;A=0;B=1;
#20;A=1;
#20;
$display("testing OR");
#20;alu_function=2'h1;A=0;B=0;
#20;A=1;
#20;A=0;B=1;
#20;A=1;
#20;
$display("test inverter");
#20;alu_function=2'h2;A=0;B=0;
#20;B=1;
#40;
end


initial
begin
$monitor("time = %2d, A=%15b,B=%15b,carryin=%1b,alu_function=%2b,alu_output=%15b,carryout=%1b",
$time,A,B,carryin,alu_function,alu_output,carryout);
end

endmodule
