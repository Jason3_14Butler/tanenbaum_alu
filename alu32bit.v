/*
  © Copywrite Jason Butler 2017 GPL
*/

module alu32bit
(
  input [31:0] A,
  input [31:0] B,
  input invA,
  input enA,
  input enB,
  input carryin,
  input [1:0] alu_function,
  output wire [31:0] alu_output,
  output wire carryout,
  output wire N,
  output wire Z
);

wire [32:0] carries;
wire [31:0] alu_output_temp;
assign carries[0] = carryin;
assign N = 0;
//assign Z = 0;

genvar i;
generate 
  for (i=0; i<32; i =i+1) begin:bitslice_alus
    bitslice_alu s(A[i], B[i],invA,enA,enB,carries[i],alu_function,alu_output_temp[i],carries[i+1]);
end endgenerate
//assign Z = 1;
//or U1(Z,alu_output_temp[0],alu_output_temp[1]);
not U1(Z,alu_output[0]|alu_output[1]|alu_output[2]|alu_output[3]|alu_output[4]|alu_output[5]|alu_output[6]|alu_output[7]|alu_output[8]|alu_output[9]|alu_output[10]|alu_output[11]|alu_output[12]|alu_output[13]|alu_output[14]|alu_output[15]|alu_output[16]|alu_output[17]|alu_output[18]|alu_output[19]|alu_output[20]|alu_output[21]|alu_output[22]|alu_output[23]|alu_output[24]|alu_output[25]|alu_output[26]|alu_output[27]|alu_output[28]|alu_output[29]|alu_output[30]|alu_output[31]); 
assign carryout = carries[31];
assign N = alu_output_temp[31];
assign alu_output = alu_output_temp;
endmodule
