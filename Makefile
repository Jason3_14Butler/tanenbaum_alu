#
#  © Copywrite Jason Butler 2017 GPL
#

all : alu16bittest

alu16bittest : alu16bit.v alu16tb.v
	iverilog -o alu16bittest alu16bit.v alu16tb.v bitslice_alu/bitslice_alu.v bitslice_alu/decoder/decoder.v bitslice_alu/fulladder/fulladder.v bitslice_alu/logic_unit/logic_unit.v

run : alu16bittest
	vvp alu16bittest

alu32 : alu32bittest

alu32bittest : alu32bit.v alu32tb.v
	iverilog -o alu32bittest alu32bit.v alu32tb.v bitslice_alu/bitslice_alu.v bitslice_alu/decoder/decoder.v bitslice_alu/fulladder/fulladder.v bitslice_alu/logic_unit/logic_unit.v 

run32 : alu32bittest
	vvp alu32bittest 
